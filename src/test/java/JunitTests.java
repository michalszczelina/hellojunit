import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JunitTests {

    private AndroidDriver driver;

    @BeforeEach
    public void beforeEachTest() throws MalformedURLException {
        DesiredCapabilities capabilities = getCapabilities();
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
    }

    @DisplayName("First test")
    @Test
    public void firstTest() {
        System.out.println("Test 1");
        assertEquals(1, 2, "Actual value is not equal to the expected value!");
    }

    @DisplayName("Second test")
    @Test
    public void secondTest() {
        System.out.println("Test 2");
        assert(true);
    }

    @DisplayName("Third test")
    @Test
    public void thirdTest() {
        System.out.println("Test 3");
        assert(true);
    }

    @DisplayName("Fourth test")
    @Test
    public void fourthTest() {
        System.out.println("Test 4");
        assert(true);
    }

    @AfterEach
    public void afterEachTest() {
        driver.quit();
    }

    private DesiredCapabilities getCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "3300a98814a193b1");
        capabilities.setCapability("app", "C:\\AndroidApps\\testapp.apk");

        return capabilities;
    }
}